/* envio de formularios do site via ajax */


var valEmail = /^[\w-]+(\.[\w-]+)*@(([A-Za-z\d][A-Za-z\d-]{0,61}[A-Za-z\d]\.)+[A-Za-z]{2,6}|\[\d{1,3}(\.\d{1,3}){3}\])jQuery/;

function sendValidate(id, lang) {

    lang = lang || 'br';
    
    if(lang == "pt-PT"){
        msgObrigatorio = "Os seguintes campos encontram-se com problemas: <br/></br>";
        msgErro = "Erro de trasmissão. Tente novamente. ";
        msgOk = "Solicitação enviada com sucesso."
        enviando = "Enviando...";
        aviso = "Atenção!";
        sucesso = "Sucesso!";
        erro = "Erro!";
    }else if(lang == "en-US"){
        msgObrigatorio = "The following fields are troubled: <br/></br>";
        msgErro = "Transmission error. Try again. ";
        msgOk = "Successfully sent request.";
        enviando = "Sending...";
        aviso = "Caution!";
        sucesso = "Success";
        erro = "Error!";
    }else if(lang == "esp"){
        msgObrigatorio = "Los siguientes campos están en problemas: <br/></br>";
        msgErro = "Error de transmisión. Vuelva a intentarlo. ";
        msgOk = "Petición enviado con éxito."
        enviando = "Enviando...";
        aviso = "¡Atención!";
        sucesso = "¡Éxito!";
        erro = "¡Error!";
    }
    
    msg = "";
    jQuery('#myModalForm #myModalLabel').html(enviando);
    jQuery('#myModalForm .modal-body').html('');
    jQuery('#myModalForm').modal('show');

    if (jQuery('.form' + id + ' input[type="submit"]').attr('disabled') != 'true') {

        jQuery('.form' + id + ' input, .form' + id + ' select, .form' + id + ' textarea').each(function () {

            if (jQuery(this).attr('data-required') == 'true') {
                if (jQuery(this).attr('placeholder') == jQuery(this).val() || jQuery(this).val() == '') {

                    if(lang == "pt-PT"){
                        msg += "* Campo " + jQuery(this).attr('placeholder') + " é obrigatório. <br />";
                    }else if(lang == "en-US"){
                        msg += "* " + jQuery(this).attr('placeholder') + " field is required. <br />";
                    }else if(lang == "esp"){
                        msg += "* Se requiere campo " + jQuery(this).attr('placeholder') + ". <br />";
                    }
                }
                /*if(jQuery(this).attr('name') == 'E-mail' || jQuery(this).attr('name') == 'email'){
                    if (!valEmail.test(jQuery(this).val())) {
                        msg += "* E-mail inválido. <br />";
                    }
                }*/
            }
        });

        if (msg != "") {

            msg = msgObrigatorio + msg;
            jQuery('#myModalForm #myModalLabel').html(aviso);
            jQuery('#myModalForm .modal-body').html(msg);
            return false;
        } else {
            jQuery('.form' + id + ' input[type="submit"]').attr("disabled", true);

            var form_validate = jQuery('.form' + id).serialize();
            jQuery.ajax({
                type: 'POST',
                url: jQuery('base').attr('href') + 'register_lead',
                data: form_validate,
                success: function (msg) {
                    if (msg == 'ok') {
                        jQuery('#myModalForm #myModalLabel').html(sucesso);
                        jQuery('#myModalForm .modal-body').html(msgOk);
                        jQuery('.form' + id).trigger('reset');
                    } else {
                        jQuery('#myModalForm #myModalLabel').html(erro);
                        jQuery('#myModalForm .modal-body').html(msgErro + msg);
                    }
                    jQuery('.form' + id + ' input[type="submit"]').attr("disabled", false);
                }
            });
        }
    }
    return false;
}

function enviar_form(id, lang) {
    sendValidate(id, lang);
}

/* fim do script de envio de email */
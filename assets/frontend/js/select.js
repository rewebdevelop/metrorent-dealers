$(document).ready(function(){ 

	$('select.select').each(function(){
		var title = $(this).attr('title');
		if( $('option:selected', this).val() != ''  ) title = $('option:selected',this).text();
		$(this)
		.css({'z-index':10,'opacity':0,'-khtml-appearance':'none'})
		.after('<span class="select">' + title + '</span>')
		.change(function(){
			val = $('option:selected',this).text();
			$(this).css({'color':'#000000'});
			$(this).next().text(val);
		})
	});

	$('.img-menu').on('click',function(){
        $('.menu-ul').slideToggle();
        $('.img-menu').toggleClass('menu-aberto');
    });

	if(navigator.userAgent.indexOf('Mac') > 0)
	$('body').addClass('mac-os');
	$('.style-select').each(function(){
		$(this).change(function(){
			$(this).css({'color':'#000000'});
		})
	});

	$('.date').each(function(){
		$(this).focus(function(){
			$(this).css({'color':'#000000'});
		})
	});

});
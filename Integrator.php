<?php

namespace Integrator;

class API {

    private $_allcars = 'car/allcars';
    private $_caryears = 'car/modelyearsrange';
    private $_carmakers = 'brand/get';
    private $_allattrs = 'car/attr';
    private $_search = 'car/search';
    private $_cardetail = 'car/detail';
    private $_filters = 'car/filter';

    //list of params and default values
    private $_availableSearchParams = array(
        'limit' => 20,
        'offset' => 0,
        'keyword' => '',
        'type' => '',
        'category' => '',
        'segment' => '',
        'year_model_min' => '',
        'year_model_max' => '',
        'odometer' => '',
        'odometer_min' => '',
        'odometer_max' => '',
        'price_min' => '',
        'price_max' => '',
        'price_transfer_min' => '',
        'price_transfer_max' => '',
        'price_max' => '',
        'title' => '',
        'description' => '',
        'brand' => '',
        'brand_model' => '',
        'brand_model_version' => '',
        'brand_id' => '',
        'brand_model_id' => '',
        'brand_model_version_id' => '',
        'attr' => '',
        'tags' => '',
        'reference' => '',
        'highlights' => '',
        'door' => '',
        'fuel_id' => '',
        'fuel' => '',
        'transmission_id' => '',
        'transmission' => '',
        'color_id' => '',
        'color' => '',
        'order_field' => 'title',
        'order_type' => 'ASC'
    );

    public function __construct($token = FALSE){
        $this->host = 'http://integrador.reweb.com.br/api/';
        $this->token = $token;
        $this->integrator = ''; //landingpage_seminovos
    }

    public function getCarYearsRange(){
        return $this->response($this->_caryears);
    }

    public function getCarMakers(){
        return $this->response($this->_carmakers);
    }

    public function getAllAttrs(){
        return $this->response($this->_allattrs);   
    }

    public function getCars($args){
        $page = 1;
        if (isset($args['page'])) {
            $page = $args['page'];
        }

        unset($args['page']);

        if ($this->validParams($args)) {
            $params = $this->assign($args);
                
            $params['offset'] = $params['limit'] * ($page-1);
            $response = $this->response($this->_search . '/' . $params['limit'] . '/' . $params['offset'], $params);

            if (isset($response->total)) {
                $response->pagination = $this->pagination($response->total, $params);
                return $response;
            }else{
                return false;
            }
            
            
        }

        return false;
        
    }

    public function getCar($car_id){
        if (empty($car_id)) {
            return false;
        }

        return $this->response($this->_cardetail . '/' . $car_id);
    }

    public function getFilters($args){
        $page = 1;
        if (isset($args['page'])) {
            $page = $args['page'];
        }

        unset($args['page']);

        if ($this->validParams($args)) {
            $params = $this->assign($args);
            $params['offset'] = $params['limit'] * ($page-1);
            $response = $this->response($this->_filters . '/' . $params['limit'] . '/' . $params['offset'], $params);
            return $response;
            
        }

        return false;

    }

    private function response($endpoint, $params = array()){
        $response = $this->postRequest($endpoint, $params);

        if (isset($response->response)) {
            if ($response->response->status->code != 0) {
                return array('error' => $response->response->status->message);
            }

            return $response->response;
        }else{
            return false;
        }
    }

    private function validParams($args){
        foreach (array_keys($args) as $key) {
            //if any param is not in the params list, return false
            if (!empty($key)) {
                if (!array_key_exists($key, $this->_availableSearchParams)) {
                    return false;
                }
            }
        }

        return true;
    }


    //this function merge the args with the default params value
    private function assign($args){
        $params = array();
        foreach ($this->_availableSearchParams as $key => $value) {
            if (isset($args[$key])) {
                if (!empty($args[$key])) {
                    $params[$key] = $args[$key]; //sent value
                }else{
                    if (!empty($value)) {
                        $params[$key] = $value; // default value
                    }
                }
            }else{
                if (!empty($value)) {
                    $params[$key] = $value; // default value
                }
            }
        }
        return $params;
    }

    private function pagination($total, $params){
        $totalPages = ceil($total/$params['limit']);
        $currentPage = ($params['offset']/$params['limit'])+1;
        $last = false;
        $first = false;

        if ($currentPage == $totalPages) {
            $last = true;
        }

        if ($currentPage == 1) {
            $first = true;
        }

        return array('totalPages' => $totalPages, 'currentPage' => $currentPage, 'lastPage' => $last, 'firstPage' => $first);
    }

    private function postRequest($endpoint, $params = array()){
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $this->host . $endpoint);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_POST, 1);

        $_params = array(
            'token' => $this->token,
            'integrator' => $this->integrator
        );

        foreach ($params as $key => $value) {
            $_params[$key] = $value;
        }

        curl_setopt($curl, CURLOPT_POSTFIELDS, $_params);
        $result = curl_exec($curl);
        curl_close($curl);
        return json_decode($result);
    }

}
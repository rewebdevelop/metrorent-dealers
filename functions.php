<?php
require_once __DIR__.'/config.php';

/*
    Return the default data sent to view
*/
// debug($ENDPOINTS);
function defaultData($app){
    global $ENDPOINTS;
    $accessToken = isset($_COOKIE['acesso_token']) ? $_COOKIE['acesso_token'] : '';
    return array(
        'rooturl' => ROOT_URL,
        'theme' => THEME_NAME,
        'lang' => $app['translator']->trans('lang'),
        'langs' => (array)getRequest($ENDPOINTS['translations'], $app['debug']),
        'menu' => getRequest($ENDPOINTS['menu'], $app['debug']),
        'editMode' => isset($_GET['editMode']) ? true : false,
        'editToken' => getEditToken(),
        'categories' => getRequest($ENDPOINTS['car_categories'], $app['debug']),
        'url_complement' => URL_COMPLEMENT,
        'url_used_cars' => URL_USED_CARS,
        'url_new_cars' => URL_NEW_CARS,
        'stores' => getRequest($ENDPOINTS['stores'], $app['debug']),
        'url_served_areas' => URL_SERVED_AREAS,
        'url_used_cars' => URL_USED_CARS,
        'url_new_cars' => URL_NEW_CARS,
        'accessToken' => $accessToken
    );
}

/*
    Return the default debug code
*/
function debug($var){
    echo '<pre>';
    print_r($var);
    die;
}

function getToken(){
    return sha1(CLIENT_KEY . sha1(SECRET_KEY) . sha1(date('Y-m-d')));
}

function getEditToken(){
    return sha1(CLIENT_KEY . sha1(md5(SECRET_KEY) . '$EDITMODE$') . sha1(date('Y-m-d')));
}

function getRequest($endpoint, $debug = false){
    //check from cache response and return if available and valid
    $resultCache = checkCache($endpoint);
    if ($resultCache !== FALSE && !$debug) { //is debug mode enable, dont use cache
        return $resultCache;
    }

    $curl = curl_init();
    curl_setopt($curl, CURLOPT_URL, $endpoint);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curl, CURLOPT_HTTPHEADER, array(
        'AutenticationToken: ' . getToken(),
        'ClientToken:' . CLIENT_KEY
    ));
    
    $result = curl_exec($curl);
    curl_close($curl);

    makeCache($endpoint, $result);
    return json_decode($result);
}

function postRequest($endpoint, $params = array()){
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_URL, $endpoint);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curl, CURLOPT_HTTPHEADER, array(
        'AutenticationToken: ' . getToken(),
        'ClientToken:' . CLIENT_KEY
    ));
    
    curl_setopt($curl, CURLOPT_POST, 1);
    curl_setopt($curl, CURLOPT_POSTFIELDS, $params);

    $result = curl_exec($curl);
    curl_close($curl);
    return json_decode($result);
}

function slugify($text)
{ 
    // replace non letter or digits by -
    $text = preg_replace('~[^\\pL\d]+~u', '-', $text);

    // trim
    $text = trim($text, '-');

    // transliterate
    $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

    // lowercase
    $text = strtolower($text);

    // remove unwanted characters
    $text = preg_replace('~[^-\w]+~', '', $text);

    if (empty($text))
    {
        return 'n-a';
    }

    return $text;
}

function checkCache($endpoint){
    $cacheDir = __DIR__ . '/cache/';

    if (!file_exists($cacheDir)) {
        mkdir($cacheDir);
    }
    
    $cacheFileName = md5($endpoint) . '.json';

    if (file_exists($cacheDir . $cacheFileName)) {
        //cache file exists, check the date of cache
        // $dateTimeFile = filemtime($cacheDir . $cacheFileName);
        // $dateTimeNow = time();
        // $timeDiff = ($dateTimeNow - $dateTimeFile)/60/60;

        // if ($timeDiff < CACHE_HOURS) {
            //the cache is not expired    
            return json_decode(file_get_contents($cacheDir . $cacheFileName));
        // }

        // return false;
    }else{
        return false;
    }
}

function makeCache($endpoint, $result){
    $cacheDir = __DIR__ . '/cache/';
    $cacheFileName = md5($endpoint) . '.json';

    if (file_exists($cacheDir . $cacheFileName)) {
        //if cache file exists, remove
        unlink($cacheDir . $cacheFileName);
    }
    file_put_contents($cacheDir . $cacheFileName, $result);
}
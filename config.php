<?php
define('THEME_NAME', "Metrorent");
define('CLIENT_KEY', "6e97fd60351ab86e351e825d78818d70");
define('SECRET_KEY', "z3dpoByS063HtXAIK6UqXGBDqycEj7Ps");
define('INTEGRATOR_KEY', "OnFscdewzTCONV1BmrJFpCuo");
//define('ROOT_URL', "http://dealers.rewebmkt.com/homologacao/metrorent/");
//

if($_SERVER['HTTP_HOST'] == 'localhost'){ 
    define('ROOT_URL', "http://localhost/metrorent-dealers/");
}elseif($_SERVER['HTTP_HOST'] == 'interface.reweb.com.br'){ 
    define('ROOT_URL', "http://interface.reweb.com.br/html/metrorentdealers/preview/");
}else{ 
    define('ROOT_URL', "http://metrorent.pt/");
}
define('CONFIG_DOMINIO', "www.metrorent.pt");
define('DOCUMENT_ROOT', __DIR__);
define('CACHE_HOURS', 24);
define('EDIT_MODE', false);
define('CONTACT_EMAIL', "mrocha@reweb.com.br");
define('DEBUG_MODE', false);
define('URL_COMPLEMENT', "em-senhora-da-hora-porto-santa-maria-da-feira");
define('URL_NEW_CARS', "metrorent");
define('URL_USED_CARS', "viaturas");
define('URL_USED_CARS_DETAIL', "viatura");
define('URL_SERVED_AREAS', 'regioes-atendidas');
define('URL_SERVED_AREAS_DETAIL', 'concessionaria-autorizada');

$apiBase = "http://dealers.rewebmkt.com/api/";
$crmBase = "http://crm2.reweb.com.br/api/websites/";

$ENDPOINTS = array(
    'banners' => $crmBase . 'banners/',
    'highlights' => $crmBase . 'highlights/',
    'cities' => $crmBase . 'cities/',
    'stores' => $crmBase . 'stores/',
    'news' => $crmBase . 'news/',
    'blog_car' => $crmBase . 'news_car/',
    'register_coment' => $crmBase .'register_coment/',
    'categories' => $crmBase . 'news_categories/',
    'category_news' => $crmBase . 'category_news/',
    'new_cars' => $crmBase . 'cars/',
    'translations' => $apiBase . 'client/site/languages',
    'forms' => $apiBase . 'form/',
    'menu' => $apiBase . 'client/site/menu',
    'page' => $apiBase . 'client/site/page/',
    'car_categories' => $apiBase . 'client/cars/categories',
    'car_detail' => $crmBase . 'cars/',
    'files' => $apiBase . 'client/site/files',
    'images' => $apiBase . 'client/site/images',
    'saveContentText' => $apiBase . 'client/site/saveTranslationText'
);
<?php
require_once __DIR__.'/vendor/autoload.php';
require_once __DIR__.'/config.php';
require_once __DIR__.'/functions.php';
require_once __DIR__.'/Integrator.php';
session_start();
date_default_timezone_set('America/Sao_Paulo');

ini_set('display_errors', '0');
$httphost = $_SERVER['HTTP_HOST'];
$_SERVER['HTTP_HOST'] = CONFIG_DOMINIO;
require_once __DIR__.'/AdManagerAPI.class.php';
$admanager = new AdManagerAPI();
$admanager->registraAcesso();
$_SERVER['HTTP_HOST'] = $httphost;

ini_set('display_errors', '0');
error_reporting(0);
// echo 'dfdfd';
// die;
$app = new Silex\Application();
$app['debug'] = DEBUG_MODE;


$app->get('/lang/{lang}', function($lang) use($app, $ENDPOINTS){
    $_SESSION['lang-'.CLIENT_KEY] = $lang;
    return $app->redirect(ROOT_URL . (isset($_GET['editMode']) ? '?editMode=true' : ''));
});


/*
    Register translation system
*/
$app->register(new Silex\Provider\TranslationServiceProvider(), array(
    'locale_fallbacks' => array('pt-pt'),
    'locale' => isset($_SESSION['lang-'.CLIENT_KEY]) ? $_SESSION['lang-'.CLIENT_KEY] : 'pt-pt',
));


/*
    Register TWIG template view system
*/
$app->register(new Silex\Provider\TwigServiceProvider(), array(
    'twig.path' => __DIR__.'/views/',
));

/*
    Register HTTP cache
*/
$app->register(new Silex\Provider\HttpCacheServiceProvider(), array(
    'http_cache.cache_dir' => __DIR__.'/cache/',
));


$translations = getRequest($ENDPOINTS['translations'], true);
// debug($translations);
$messages = array();
$defaultLang = '';
foreach ($translations as $lang => $words) {
    // $dict = array();
    if ($defaultLang == '') {
        $defaultLang = $lang;
    }

    foreach ($words as $word) {
        foreach((array)$word as $key => $value){
            $messages[$lang][$key] = $value;
        }
    }
}

// load custom files and images

$files = getRequest($ENDPOINTS['files'], $app['debug']);
foreach ($files as $file) {
    $assetCustomDir = __DIR__.'/assets/custom/';
    if (!empty($file->file_name)) {
        file_put_contents($assetCustomDir . $file->file_name, $file->content);
    }
}

$images = getRequest($ENDPOINTS['images'], $app['debug']);
foreach ($images as $image) {
    $assetCustomDir = __DIR__.'/assets/custom/img/';
    if (!empty($image->file) && !empty($image->content)) {
        if (!file_exists($assetCustomDir . $image->file)) {
            file_put_contents($assetCustomDir . $image->file, base64_decode($image->content));
        }
    }
}

//debug($messages);
$app['translator.domains'] = array(
    'messages' => $messages
);

/*
    CREATE THE FORM FILTER
*/
$app['twig'] = $app->share($app->extend('twig', function($twig, $app) use($ENDPOINTS, $defaultLang, $app) {
    $filter = new Twig_SimpleFilter('makeform', function ($formname) use($ENDPOINTS, $defaultLang, $app) {
        $lang = isset($_SESSION['lang-'.CLIENT_KEY]) ? $_SESSION['lang-'.CLIENT_KEY] : $defaultLang;
        $form = getRequest($ENDPOINTS['forms'] . $formname . '/' .  $lang, $app['debug']);

        if (isset($form->html)) {
            $form = str_replace('{{ rooturl }}/', ROOT_URL, $form->html);
            return $form;
        }else{
            return $form->error;
        }
    });

    $slug_filter = new Twig_SimpleFilter('slugfy', function ($string) {
        return slugify($string);
    });

    $new_car_url = new Twig_SimpleFilter('url', function ($newCar) {
        return ROOT_URL . URL_NEW_CARS . (URL_NEW_CARS ? '-' : '') . $newCar->slug . '-' . URL_COMPLEMENT;
    });

    $detail_served_areas_url = new Twig_SimpleFilter('detail_served_areas_url', function ($city_slug) {
        return ROOT_URL . URL_SERVED_AREAS_DETAIL . (URL_SERVED_AREAS_DETAIL ? '-' : '') . $city_slug . '-rio-de-janeiro-rj';
    });

    $used_car_url = new Twig_SimpleFilter('used_url', function ($usedCar) {
        return ROOT_URL . URL_USED_CARS_DETAIL . '-' . slugify($usedCar->brand) . '-' . slugify($usedCar->title) . '-cod_' . $usedCar->id;
    });

    $twig->addFilter($slug_filter);
    $twig->addFilter($filter);
    $twig->addFilter($new_car_url);
    $twig->addFilter($used_car_url);
    $twig->addFilter($detail_served_areas_url);
    return $twig;
}));

/*
    Default page - Home
*/
$app->get('/', function() use($app, $defaultLang, $ENDPOINTS){

    $admanager = new AdManagerAPI();
    $unidades = $admanager->getUnidadesCliente('');

    $data = defaultData($app);
    $integrator = new \Integrator\API(INTEGRATOR_KEY);

    $banners = getRequest($ENDPOINTS['banners'], $app['debug']);

    $lang = isset($_SESSION['lang-'.CLIENT_KEY]) ? $_SESSION['lang-'.CLIENT_KEY] : $defaultLang;

    foreach ($banners->banners as $banner) {
        if(strtoupper($banner->lang)==strtoupper($lang))
            $banners_lang[] = $banner;
    }

    //die($_SESSION['lang-'.CLIENT_KEY]);
    $data['banners']->banners = $banners_lang;
    $data['stores'] = getRequest($ENDPOINTS['stores'], $app['debug']);
    $data['highlights'] = getRequest($ENDPOINTS['highlights'], $app['debug']);

    $data['page'] = 'home';
    if (isset($_SESSION['success'])) {
        $data['success'] = $_SESSION['success'];
        unset($_SESSION['success']);
    }

    $data['filters'] = $integrator->getFilters();
    $all = $integrator->getCars();
    $passenger = $integrator->getCars(array('tags' => 'LIGEIROS DE PASSAGEIROS'));
    $comerciais = $integrator->getCars(array('tags' => 'COMERCIAIS E TURISMO'));
    $highlight = $integrator->getCars(array('highlights' => 1, 'limit' => 1));
    $highlight = $integrator->getCar($highlight->data[0]->id);

    foreach ($all->data as $car) {
        if ($defaultLang != $lang){
            foreach ($car->languages as $car_lang => $translate) {
                if(strtoupper($car_lang)==strtoupper($lang)){
                    $car->description = $translate->descricao;
                    $car->price = $translate->valor;
                }
            }
        }
        $cars[] = $car;
    }

    $data['cars'] = Array(
        'passenger'  => $passenger->data,
        'comerciais' => $comerciais->data,
        'highlight'  => $highlight->data,
        'all'        => $cars
    );

    return $app['twig']->render('index.html', $data);
});

$app->get('/blog', function() use($app, $ENDPOINTS){
    $data = defaultData($app);
    $news = getRequest($ENDPOINTS['news'],'news', $app['debug']);
    $data['news'] = array_chunk($news->news, 5);
    $data['news_recents'] = $news->recents;
    $data['news_more_views'] = $news->more_views;
    $data['categories_news'] = getRequest($ENDPOINTS['categories'], $app['debug']);

    return $app['twig']->render('blog.html', $data);
});

$app->get('/blog/{new_slug}', function($new_slug) use($app, $ENDPOINTS){
    $data = defaultData($app);
    $news = getRequest($ENDPOINTS['news'],'news', $app['debug']);
    $data['news'] = $news->news;
    $data['news_recents'] = $news->recents;
    $data['news_more_views'] = $news->more_views;

    $data['new'] = getRequest($ENDPOINTS['news'].$new_slug, $app['debug']);
    $data['categories_news'] = getRequest($ENDPOINTS['categories'], $app['debug']);

    return $app['twig']->render('blog_detalhe.html', $data);
});

$app->post('/register_coment/{new}/{new_slug}', function($new, $new_slug) use($app, $ENDPOINTS){
    $data = Array(
        'name'    => $_POST['name'],
        'email'   => $_POST['email'],
        'message' => $_POST['message'],
        'new_id'  => $new
    );

    postRequest($ENDPOINTS['register_coment'], $data);
    return $app->redirect(ROOT_URL.'blog/'.$new_slug);
});

$app->get('/blog/category/{category_slug}', function($category_slug) use($app, $ENDPOINTS){
    $data = defaultData($app);
    $data['categories_news'] = getRequest($ENDPOINTS['categories'], $app['debug']);
    $news = getRequest($ENDPOINTS['category_news'].$category_slug,'news', $app['debug']);
    $data['news'] = array_chunk($news->news, 5);

    $news_geral = getRequest($ENDPOINTS['news'],'news');
    $data['news_recents'] = $news_geral->recents;
    $data['news_more_views'] = $news_geral->more_views;

    return $app['twig']->render('blog.html', $data);
});


$app->get('/' . URL_SERVED_AREAS, function() use($app, $ENDPOINTS){
    $data = defaultData($app);
    $cities = getRequest($ENDPOINTS['cities'], $app['debug']);
    $data['cities'] = $cities->cities;

    return $app['twig']->render('served_areas.html', $data);
});

$app->get('/' . URL_SERVED_AREAS_DETAIL . '-{city_slug}', function($city_slug) use($app, $defaultLang, $ENDPOINTS){
    $data = defaultData($app);
    $data['city'] = getRequest($ENDPOINTS['cities'] . $city_slug, $app['debug']);

    return $app['twig']->render('served_areas_detail.html', $data);
})->assert('city_slug', '.*');


function searchUsedCars($args, $app, $ENDPOINTS){

    $data = defaultData($app);
    $integrator = new \Integrator\API(INTEGRATOR_KEY);

    $all = $integrator->getCars();
    $passenger = $integrator->getCars(array('tags' => 'LIGEIROS DE PASSAGEIROS'));
    $comerciais = $integrator->getCars(array('tags' => 'COMERCIAIS E TURISMO'));
    $highlight = $integrator->getCars(array('highlights' => 1, 'limit' => 1));
    $data['car'] = $integrator->getCar($highlight->data[0]->id);

    $gallery = $data['car']->data->gallery;
    $photo = '';
    foreach ($gallery as $item) {
        if (!$photo) {
            $photo = $item->file;
        }
    }

    $data['photo'] = $photo;

    $data['car']->data->price = number_format($data['car']->data->price, 2, ',', '.');
    $data['car']->data->tag = 'CARRO DO MÊS';

    $lang = isset($_SESSION['lang-'.CLIENT_KEY]) ? $_SESSION['lang-'.CLIENT_KEY] : $defaultLang;

    if ($defaultLang != $lang){
        foreach ($data['car']->data->languages as $car_lang => $translate) {
            if(strtoupper($car_lang)==strtoupper($lang)){
                $data['car']->data->description = $translate->descricao;
                $data['car']->data->price = $translate->valor;
            }
        }
    }


     $data['cars'] = Array(
        'passenger'  => $passenger->data,
        'comerciais' => $comerciais->data,
        'highlight'  => $data['car']->data
    );


    //debug($data['photo']);


    //debug($integrator->getCarYearsRange());
    return $app['twig']->render('viaturas.html', $data);
}



/*
    USED CAR DETAIL
*/
$app->get('/' . URL_USED_CARS_DETAIL . '-{car_slug}-cod_{car_id}', function($car_slug, $car_id) use($app, $defaultLang, $ENDPOINTS){
    $data = defaultData($app);
    $integrator = new \Integrator\API(INTEGRATOR_KEY);

    $passenger = $integrator->getCars(array('tags' => 'LIGEIROS DE PASSAGEIROS'));
    $comerciais = $integrator->getCars(array('tags' => 'COMERCIAIS E TURISMO'));
    $highlight = $integrator->getCars(array('highlights' => 1, 'limit' => 1));



    $data['car'] = $integrator->getCar($car_id);
    // debug($data['car']);
    $gallery = $data['car']->data->gallery;
    $photo = '';
    foreach ($gallery as $item) {
        if (!$photo) {
            $photo = $item->file;
        }
    }


    $data['photo'] = $photo;

    $data['car']->data->price = number_format($data['car']->data->price, 2, ',', '.');

    $data['cars'] = Array(
        'passenger'  => $passenger->data,
        'comerciais' => $comerciais->data,
        'highlight'  => $highlight->data[0],
    );

    $data['car']->data->tag = $data['car']->data->tags[0]->title;

    $lang = isset($_SESSION['lang-'.CLIENT_KEY]) ? $_SESSION['lang-'.CLIENT_KEY] : $defaultLang;

    if ($defaultLang != $lang){
        foreach ($data['car']->data->languages as $car_lang => $translate) {
            if(strtoupper($car_lang)==strtoupper($lang)){
                $data['car']->data->description = $translate->descricao;
                $data['car']->data->price = $translate->valor;
            }
        }
    }


    return $app['twig']->render('viaturas.html', $data);
})->assert('car_slug', '.*')
->assert('car_id', '^\d+$');


$app->get('/' . URL_USED_CARS . '-' . URL_COMPLEMENT, function() use($app, $ENDPOINTS){
    return searchUsedCars(array(), $app, $ENDPOINTS);
});

$app->get('/' . URL_USED_CARS . '-{fake_args}-' . URL_COMPLEMENT . '-{args}', function($args) use($app, $ENDPOINTS){
    return searchUsedCars($args, $app, $ENDPOINTS);
})->assert('args', '.*')
->assert('fake_args', '.*')
->convert('args', function($args){
    $args = explode('-', $args);
    $params = array();

    foreach ($args as $arg) {
        if (count(explode('_', $arg)) > 1) {
            list($name, $value) = explode('_', $arg);
            $params[implode('_', explode('+', $name))] = implode('_', explode('+', $value));
        }
    }

    return $params;
});



$app->get('/' . URL_NEW_CARS, function() use($app, $ENDPOINTS){
    $data = defaultData($app);
    $data['categories'] = getRequest($ENDPOINTS['car_categories'], $app['debug']);
    // debug($data);
    return $app['twig']->render('new_cars.html', $data);
});

/*
    Default page - Veículos novos - Detalhe
*/
$app->get('/' . URL_NEW_CARS . (URL_NEW_CARS ? '-' : '') . '{car_slug}-' . URL_COMPLEMENT, function($car_slug) use($app, $ENDPOINTS){
    $data = defaultData($app);
    $data['car'] = getRequest($ENDPOINTS['car_detail'] . $car_slug, $app['debug']);

    $news = getRequest($ENDPOINTS['blog_car'] . $data['car']->id, $app['debug']);
    $data['news'] = $news->news;
    $data['news_recents'] = $news->recents;

    return $app['twig']->render('car_detail.html', $data);
})->assert('car_slug', '.*');

/*
    Custom pages
*/
$app->get('/{page}', function($page) use($app, $ENDPOINTS){
    $data = defaultData($app);
    $page = getRequest($ENDPOINTS['page'] . urlencode($page), $app['debug']);

    if (!isset($page->url)) {
        $data['title'] = 'Página não encontrada';
        $data['message'] = 'A página solicitada não foi encontrada.';
        //TODO get the pages for the client

        return $app['twig']->render('error.html', $data);
    }

    if (!file_exists(__DIR__. '/views/temp/')) {
        mkdir(__DIR__. '/views/temp/');
    }

    if (!file_exists(__DIR__. '/views/temp/' . $page->url . '.html')) {
        file_put_contents(__DIR__.'/views/temp/' . $page->url . '.html', $page->content);
    }else if($app['debug']){
        file_put_contents(__DIR__.'/views/temp/' . $page->url . '.html', $page->content);
    }

    $data['page'] = 'temp/' . $page->url;
    $data['title'] = $page->title;
    $data['keywords'] = $page->keywords;
    $data['description'] = $page->description;
    $data['h1'] = $page->h1;
    $data['alt'] = $page->alt;
    //TODO get the pages for the client
    return $app['twig']->render('page.html', $data);
});


$app->post('/save-content-text', function() use($app, $ENDPOINTS){
    $content = trim($_POST['content']);
    $key = $_POST['key'];
    $lang = $_POST['lang'];
    $response = postRequest($ENDPOINTS['saveContentText'], array('content' => $content, 'key' => $key, 'lang' => $lang));
    return json_encode($response);
});

/*

*/
$app->post('/register_lead', function() use($app, $ENDPOINTS){

    $trans = array('name' => 'Nome', 'phone' => 'Telefone', 'message' => 'Mensagem');

    $meio_captacao = $_POST['formName'];
    $data = $_POST;
    $name = '';
    $email = '';
    $phone = '';
    $city = '';
    $state = '';
    $unit = '';

    unset($data['formName']);
    $message = '';
    foreach($data as $field => $value){
        if (isset($trans[$field])) {
            $field = $trans[$field];
        }

        $message .= '<b>' . ucfirst(strtolower($field)) . '</b>' . ': ' . $value . '</br>';
    }

    if (isset($data['name'])) {
        $name = $data['name'];
        unset($data['name']);
    }

    if (isset($data['email'])) {
        $email = $data['email'];
        unset($data['email']);
    }

    if (isset($data['phone'])) {
        $phone = $data['phone'];
        unset($data['phone']);
    }

    if (isset($data['city'])) {
        $city = $data['city'];
        unset($data['city']);
    }

    if (isset($data['state'])) {
        $state = $data['state'];
        unset($data['state']);
    }

    if (isset($data['unidade'])) {
        $unit = $crmUnits[$data['unidade']];
        unset($data['unidade']);
    }

    $meio_captacao = $_POST['formName'];

    ini_set('display_errors', '0');
    $admanager = new AdManagerAPI();
    $admanager->registraLead($meio_captacao,$name,$email,$phone,$city,$state,'Portugal',$message);
    ini_set('display_errors', '0');
    $_SESSION['success'] = true;

    return "ok";
    });


/*
 SEND EMAIL
*/
$app->post('/send_mail', function() use($app, $ENDPOINTS){
    $name = $_POST['name'];
    $phone = $_POST['phone'];

    $message = 'Olá! <br><br>';
    $message .= 'Segue abaixo os dados do usuário:<br>';
    $message .= '<b>Nome: </b>' . $name . '<br>';
    $message .= '<b>Telefone: </b>' . $phone . '<br>';

    $headers = "MIME-Version: 1.0\r\n";
    $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";

    if ($name != '' && $phone != ''){
        if (mail(CONTACT_EMAIL, 'Requisição de Ligação', utf8_decode($message), $headers)) {
            return 'ok';
        }else{
            return 'error';
        }
    }

    return 'null';

});

/*
    Custom error handler
*/
$app->error(function (\Exception $e, $code) use($app) {
    if (DEBUG_MODE) {
        return $e;
    }

    $data = defaultData($app);
    $data['title'] = 'Página não encontrada';
    $data['message'] = 'A página solicitada não foi encontrada.';

    //TODO get the pages for the client
    return $app['twig']->render('error.html', $data);

    return new Response('URL não encontrada');
});

$app->run();
